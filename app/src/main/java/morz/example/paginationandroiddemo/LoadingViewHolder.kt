package morz.example.paginationandroiddemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class LoadingViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(phoneNumberModel: PhoneNumberModel) {
    }
    companion object {
        fun create(parent: ViewGroup): LoadingViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.loading_layout, parent, false)
            return LoadingViewHolder(view)
        }
    }
}