package morz.example.paginationandroiddemo

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class PhoneNumberListAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var phoneNumberList: ArrayList<PhoneNumberModel> = ArrayList()
        get() = field
        set(value) {
            field = value
        }
    var endReached = false

    fun setData(value: ArrayList<PhoneNumberModel>) {
        phoneNumberList.addAll(value)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        if (position < phoneNumberList.size) {
            return 0
        } else if (!endReached && position == phoneNumberList.size) {
            return 1
        }
        return -1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> {
                PhoneNumberListViewHolder.create(parent)
            }
            else -> {
                LoadingViewHolder.create(parent)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            0 -> {
                val h = holder as PhoneNumberListViewHolder
                h.bind(phoneNumberList[position])
            }
            else -> {
                val h = holder as LoadingViewHolder
            }
        }
    }

    override fun getItemCount(): Int {
        var count: Int = phoneNumberList.size
        if (phoneNumberList.isNotEmpty()) {
            if (!endReached) {
                count++
            }
        }
        return count
    }

}