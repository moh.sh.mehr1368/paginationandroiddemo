package morz.example.paginationandroiddemo

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.timerTask

class MainActivity : AppCompatActivity() {
    val phoneNumberList = ArrayList<PhoneNumberModel>()
    var phoneNumberListAdapter: PhoneNumberListAdapter? = null
    var endReached = false
    var loading = false
    var offset = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        for (i in 1..300)
            phoneNumberList.add(PhoneNumberModel("0912******", i))
        phoneNumberListAdapter = PhoneNumberListAdapter()
        recyclerView.adapter = phoneNumberListAdapter

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                Log.d("MORZ","dx: $dx")
                Log.d("MORZ","dy: $dy")
                val lm = recyclerView.layoutManager as LinearLayoutManager
                val firstVisibleItem = lm.findFirstCompletelyVisibleItemPosition()
                val visibleItemCount =
                    if (firstVisibleItem == RecyclerView.NO_POSITION) 0 else Math.abs(
                        lm.findLastVisibleItemPosition() - firstVisibleItem
                    ) + 1
                val totalItemCount: Int = phoneNumberListAdapter!!.itemCount
                if (visibleItemCount > 0) {
                    if (!endReached && firstVisibleItem + visibleItemCount >= totalItemCount && !loading ) {
                        offset += 30
                        getPhoneNumbers(30)
                    }
                }
                Log.d("MORZ","firstVisibleItem: $firstVisibleItem")
                Log.d("MORZ","visibleItemCount: $visibleItemCount")
                Log.d("MORZ","totalItemCount: $totalItemCount")
                Log.d("MORZ","offset: $offset")
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }
        })
        getPhoneNumbers(30)
    }

    private fun getPhoneNumbers(count: Int) {
        loading = true
        Timer().schedule(timerTask {
            val tempPhoneNumberList = ArrayList<PhoneNumberModel>()
            if (offset == phoneNumberList.size){
                endReached = true
                phoneNumberListAdapter?.endReached = endReached
                phoneNumberListAdapter?.notifyDataSetChanged()
            }else{

                for (i in offset until count + offset)
                    tempPhoneNumberList.add(phoneNumberList[i])
                updateUi(tempPhoneNumberList)
            }
            loading = false
        }, 3000)
    }

    private fun updateUi(data: ArrayList<PhoneNumberModel>) {
        val handler = Handler(Looper.getMainLooper())
        handler.post {
            phoneNumberListAdapter?.setData(data)
        }
    }
}