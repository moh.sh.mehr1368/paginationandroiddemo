package morz.example.paginationandroiddemo

data class PhoneNumberModel(
    var phoneNumber: String = "",
    var rowNumber: Int = 0
)