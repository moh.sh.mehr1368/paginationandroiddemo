package morz.example.paginationandroiddemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class PhoneNumberListViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    fun bind(phoneNumberModel: PhoneNumberModel) {
        val phoneNumberView = itemView.findViewById<TextView>(R.id.phoneNumber)
        val rowNumberView = itemView.findViewById<TextView>(R.id.rowNumber)
        phoneNumberModel.run {
            phoneNumberView.text = phoneNumber
            rowNumberView.text = rowNumber.toString()
        }
    }
    companion object {
        fun create(parent: ViewGroup): PhoneNumberListViewHolder {
            val view = LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item_layout, parent, false)
            return PhoneNumberListViewHolder(view)
        }
    }
}